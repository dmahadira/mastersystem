USE [master]
GO
/****** Object:  Database [DB_MASTERSYSTEM]    Script Date: 23/03/2021 23.14.37 ******/
CREATE DATABASE [DB_MASTERSYSTEM] ON  PRIMARY 
( NAME = N'DB_MASTERSYSTEM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DB_MASTERSYSTEM.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DB_MASTERSYSTEM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DB_MASTERSYSTEM_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_MASTERSYSTEM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET  MULTI_USER 
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET DB_CHAINING OFF 
GO
USE [DB_MASTERSYSTEM]
GO
/****** Object:  Table [dbo].[tbl_error_log]    Script Date: 23/03/2021 23.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_error_log](
	[pid] [varchar](50) NOT NULL,
	[user_id] [varchar](50) NULL,
	[error_message] [varchar](8000) NULL,
	[action_method] [varchar](5000) NULL,
	[action_controller] [varchar](5000) NULL,
	[module] [varchar](50) NULL,
	[created_date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_user]    Script Date: 23/03/2021 23.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user](
	[pid] [varchar](50) NOT NULL,
	[full_name] [varchar](200) NULL,
	[password] [varchar](50) NULL,
	[phone_number] [varchar](50) NULL,
	[email] [varchar](200) NULL,
	[email_confirmation] [bit] NULL,
	[gender] [varchar](50) NULL,
	[birth_date] [date] NULL,
 CONSTRAINT [PK_tbl_user] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_user_log]    Script Date: 23/03/2021 23.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user_log](
	[log_id] [varchar](50) NOT NULL,
	[user_id] [varchar](50) NULL,
	[action_method] [varchar](5000) NULL,
	[action_controller] [varchar](5000) NULL,
	[module] [varchar](50) NULL,
	[log_date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_user]    Script Date: 23/03/2021 23.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vw_user]

as
SELECT [pid]
      ,[full_name]
      ,[password]
      ,[phone_number]
      ,[email]
      ,[email_confirmation]
      ,[gender]
      ,convert(varchar(15), [birth_date]) as birth_date
  FROM [DB_MASTERSYSTEM].[dbo].[tbl_user]
GO
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_pid]  DEFAULT (CONVERT([varchar](50),newid(),(0))) FOR [pid]
GO
/****** Object:  StoredProcedure [dbo].[cusp_ins_user]    Script Date: 23/03/2021 23.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[cusp_ins_user]
	@fullName as varchar(200),
	@email as varchar(200),
	@phoneNumber as varchar(50),
	@password as varchar(200),
	@gender as varchar(50),
	@birthDate as varchar(50),

	@status as bit OUTPUT,
	@messages as varchar(2000) OUTPUT
as
begin
declare @encryptPassword as varchar(2000) set @encryptPassword = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)
		
		set @phoneNumber = REPLACE(@phoneNumber,'+', '')
		set @phoneNumber = REPLACE(@phoneNumber,' (', '')
		set @phoneNumber = REPLACE(@phoneNumber,') ', '')
		set @phoneNumber = REPLACE(@phoneNumber,'-', '')
		set @phoneNumber = REPLACE(@phoneNumber,'_', '')

		
		insert into tbl_user(pid, full_name, email, phone_number, [password], gender, birth_date) 
		values(convert(varchar(50), newid()), upper(@fullName), upper(@email), @phoneNumber, @password, @gender, @birthDate)
		
		set @status = 1
		set @messages = 'Registrasi Berhasil.'
end

GO
USE [master]
GO
ALTER DATABASE [DB_MASTERSYSTEM] SET  READ_WRITE 
GO
