Tools yang perlu disiapkan:
- SQLServer (Saya menggunakan SQLServer 2017)
- Visual Studio Code
- Visual Studio Express/Visual Studio Community/Visual Studio Profesional dan sejenisnya.



Library/Teknologi yang digunakan:
* WebClient : - JQuery
              - Bootstrap
              - AdminSBS
              - SweetAlert2
* WebAPI    : - JWT
              - EntityFramework
              - Hashing 



Dalam repository ini terdapat 2 project dan 1 buah Database, yaitu:
- WebClient 
- WebAPI
- DB_MASTERSYSTEM (Generate Script)


Untuk menjalankan aplikasi :
1. Persiapkan Database SQLServer
2. Restore database menggunakan Generate Script yang sudah disediakan (DB_MASTERSYSTEM)
3. Persiapkan VS Code untuk menjalankan WebAPI
4. Open Folder dan Arahkan/Select Folder WebCore
5. Setelah terbuka pastikan kembali bahwa anda telah menginstall extension untuk C# Language
6. Jika sudah, terminal windows, ketikkan perintah "dotnet restore" agar library yang digunakkan dapat terimport dengan benar.
7. Lalu ketikkan perintah "dotnet run" untuk menjalankan WebAPI
8. Sekarang persiapkan Visual Studio Express
9. Buka solution WebClient, lalu run project WebClient-nya.


Jika aplikasi tidak dapat berjalan atau anda mendapatkan kesulitan, feel free to contact me:
Email       : dmahadira18@gmail.com
Whatsapp    : +62878-8070-1601
