﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebClient.Models
{
    public class ResponseModel
    {
        public bool status { get; set; }
        public string messages { get; set; }
        public Response response { get; set; }
    }

    public class Response
    {
        public string token { get; set; }
        public int expired { get; set; }
        public string expired_date { get; set; }
        public string pid_user { get; set; }
        public string email { get; set; }
        public string full_name { get; set; }
    }
}