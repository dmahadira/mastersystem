﻿using System.Web;
using System.Web.Optimization;

namespace WebClientReport
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //kendo Styles
            bundles.Add(new StyleBundle("~/bundles/kendo/css").Include(
            "~/wwwroot/Kendo/css/kendo.common.min.css",
            "~/wwwroot/Kendo/css/kendo.default.min.css",
            "~/wwwroot/Kendo/css/kendo.mobile.all.min.css"));

            //kendo scripts
            bundles.Add(new ScriptBundle("~/bundles/kendo/js").Include(
            "~/wwwroot/Kendo/js/kendo.all.min.js",
            "~/wwwroot/Kendo/js/kendo.aspnetmvc.min.js",
            "~/wwwroot/Kendo/jszip.min.js"));
            
            BundleTable.EnableOptimizations = true;
        }
    }
}
