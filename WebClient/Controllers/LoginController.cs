﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using WebClient.Helpers;
using System.Collections.Generic;
using System.Net.Http;
using WebClient.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            if (checkTokenExpired())
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public bool checkTokenExpired()
        {
            if (Session["token"] != null)
            {
                DateTime now = DateTime.Now;
                if (now >= DateTime.Parse((String)Session["expired_date"]))
                {
                    Session.Clear();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public async Task<JsonResult> SubmitLogin(UserModel param)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:5000/api/Login/");
                    //HTTP GET
                    var postTask = await client.PostAsJsonAsync<UserModel>("CreateToken", param);

                    if (postTask.IsSuccessStatusCode)
                    {
                        var resultString = await postTask.Content.ReadAsStringAsync();

                        var result = JsonConvert.DeserializeObject<ResponseModel>(resultString);

                        if (result.status)
                        {
                            Session["token"] = result.response.token;
                            Session["expired_date"] = result.response.expired_date;
                            Session["full_name"] = result.response.full_name;
                        }
                        return Json(new { status = result.status, messages = result.messages });
                    }
                    else 
                    {
                        return Json(new { status = false, messages = "Internal server error, please contact your administrator." });
                    }
                }
            }
            catch (Exception e) {
                Console.Write(e.Message);

                return Json(new { status = false, messages = "Internal server error, please contact your administrator." });
            }

        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            return RedirectToAction("index", "login");
        }
    }
}