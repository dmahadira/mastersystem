﻿using System.Web.Mvc;

namespace WebClientReport.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["token"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            
            return View();
        }
        
    }
}