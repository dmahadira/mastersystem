﻿using Newtonsoft.Json;
using WebClient.Helpers;
using WebClient.Models;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> RegisterUser(UserModel param)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:5000/api/Register/");
                    //HTTP GET
                    var postTask = await client.PostAsJsonAsync<UserModel>("RegisterUser", param);

                    if (postTask.IsSuccessStatusCode)
                    {
                        var resultString = await postTask.Content.ReadAsStringAsync();

                        var result = JsonConvert.DeserializeObject<ResponseModel>(resultString);

                        return Json(new { status = result.status, messages = result.messages });
                    }
                    else
                    {
                        return Json(new { status = false, messages = "Internal server error, please contact your administrator." });
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);

                return Json(new { status = false, messages = "Internal server error, please contact your administrator." });
            }

        }
    }
}