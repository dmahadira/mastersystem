using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.General
{
    [Table("tbl_user_log")]
    public class tbl_user_log
    {
        [Key]
        public string log_id { get; set; }
        public string user_id { get; set; }
        public string action_method { get; set; }
        public string action_controller { get; set; }
        public string module { get; set; }
        public DateTime? log_date { get; set; }
        public static void insertLog(GeneralContext sDb, String userId, String sMethod, String sController)
        {
            tbl_user_log iTbl = new tbl_user_log();

            iTbl.log_id = Guid.NewGuid().ToString();
            iTbl.user_id = userId.ToUpper();
            iTbl.action_method = sMethod;
            iTbl.action_controller = sController;
            iTbl.module = "WebCore";
            iTbl.log_date = DateTime.Now;
            sDb.Add(iTbl);
            sDb.SaveChanges();
        }
    }
}