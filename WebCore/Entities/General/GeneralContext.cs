using Microsoft.EntityFrameworkCore;

namespace Entities.General
{
    public class GeneralContext : DbContext
    {
        public DbSet<tbl_user> tbl_user { get; set; }
        public DbSet<tbl_user_log> tbl_user_log { get; set; }
        public DbSet<tbl_error_log> tbl_error_log { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(new BaseUrls().getTrainingConnectionString(), null);
        }
    }
}