using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Helpers;
using Models.General;

namespace Entities.General
{
    [Table("tbl_user")]
    public class tbl_user
    {
        [Key]
        public string pid { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone_number { get; set; }
        public string full_name { get; set; }
        public string gender { get; set; }
        public DateTime? birth_date { get; set; }

        public static tbl_user getUser(GeneralContext sDb, string email)
        {
            tbl_user iReturn = new tbl_user();
            try
            {
                iReturn = sDb.tbl_user.Where(f => f.email.Equals(email)).FirstOrDefault();

            }
            catch (Exception)
            {
            }
            return iReturn;
        }

        public static tbl_user getUserByPhone(GeneralContext sDb, string phone_number)
        {
            tbl_user iReturn = new tbl_user();
            try
            {
                iReturn = sDb.tbl_user.Where(f => f.phone_number.Equals(phone_number)).FirstOrDefault();

            }
            catch (Exception)
            {
            }
            return iReturn;
        }

        public static bool checkValidUser(GeneralContext sDb, LoginModel login)
        {
            bool iReturn = false;
            try
            {
                tbl_user iTbl = new tbl_user();
                iTbl = sDb.tbl_user.Where(f => f.email.Equals(login.email) && f.password.Equals(GenerateEncryption.GetHashPassword(login.password))).FirstOrDefault();
                if (iTbl != null)
                    iReturn = true;
            }
            catch (Exception)
            {
            }
            return iReturn;
        }
    }
}