using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models.General;

namespace Entities.General
{
    [Table("tbl_error_log")]
    public class tbl_error_log
    {
        [Key]
        public string pid { get; set; }
        public string user_id { get; set; }
        public string error_message { get; set; }
        public string action_method { get; set; }
        public string action_controller { get; set; }
        public string module { get; set; }
        public DateTime? created_date { get; set; }

        public static void insertErrorLog(GeneralContext sDb, String userId, String sMethod, String sController, String sError)
        {
            tbl_error_log iTbl = new tbl_error_log();

            iTbl.pid = Guid.NewGuid().ToString();
            iTbl.user_id = userId.ToUpper();
            iTbl.error_message = sError;
            iTbl.action_method = sMethod;
            iTbl.action_controller = sController;
            iTbl.module = "WebCore";
            iTbl.created_date = DateTime.Now;
            sDb.Add(iTbl);
            sDb.SaveChanges();
        }
    }
}