using System.IO;
using Microsoft.Extensions.Configuration;

namespace Entities
{
    public class BaseUrls
    {
        static public IConfigurationRoot Configuration { get; set; }
        public BaseUrls () {
            var builder = new ConfigurationBuilder ()
                .SetBasePath (Directory.GetCurrentDirectory ())
                .AddJsonFile ("appsettings.json");

            Configuration = builder.Build ();
        }

        public string getJwtIssuer () {
            return Configuration["Jwt:Issuer"];
        }
        public string getJwtKey () {
            return Configuration["Jwt:Key"];
        }
        public string getJwtAudience () {
            return Configuration["Jwt:Audience"];
        }
        public string getTrainingConnectionString () {
            return Configuration["TrainingConnection"];
        }
    }
}