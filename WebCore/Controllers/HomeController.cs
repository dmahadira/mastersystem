﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Controllers {
    public class HomeController : Controller {
        private readonly ILogger<HomeController> _logger;

        public HomeController (ILogger<HomeController> logger) {
            _logger = logger;
        }
        public String Index () {
            return "WebCore Service";
        }
        public String Publish () {
            return "WebCore Service Service Deployed at 23-03-2021 10:30";
        }
    }
}