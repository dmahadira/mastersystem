using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Entities.General;
using Helpers;
using Models.General;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Web.Http.Cors;

namespace Controllers
{
    [Route("api/[controller]")]
    // [EnableCors(origins: "http://localhost:5000", headers: "*", methods: "*")]
    public class RegisterController : Controller
    {
        private GeneralContext db_general;

        public RegisterController(GeneralContext db_general)
        {
            this.db_general = db_general;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("RegisterUser")]
        public async Task<JsonResult> RegisterUser([FromBody] RegisterModel register)
        {
            RouteDataClass route = RouteDataClass.getRouteData(ControllerContext);
            try
            {
                var emailExist = tbl_user.getUser(db_general, register.email);
                if (emailExist != null)
                {
                    return Json(new { status = false, messages = "Email tersebut sudah terdaftar." });
                }

                var phoneExist = tbl_user.getUserByPhone(db_general, register.phone_number);
                if (phoneExist != null)
                {
                    return Json(new { status = false, messages = "Phone Number tersebut sudah terdaftar." });
                }

                var in1 = new SqlParameter
                {
                    ParameterName = "@fullName",
                    Value = register.full_name,
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };
                var in2 = new SqlParameter
                {
                    ParameterName = "@email",
                    Value = register.email,
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };
                var in3 = new SqlParameter
                {
                    ParameterName = "@phoneNumber",
                    Value = register.phone_number,
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };
                var in4 = new SqlParameter
                {
                    ParameterName = "@password",
                    Value = GenerateEncryption.GetHashPassword(register.password),
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };
                var in5 = new SqlParameter
                {
                    ParameterName = "@gender",
                    Value = register.gender,
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };
                var in6 = new SqlParameter
                {
                    ParameterName = "@birthDate",
                    Value = register.birth_date,
                    DbType = System.Data.DbType.String,
                    Direction = System.Data.ParameterDirection.Input,
                    IsNullable = true
                };

                var out1 = new SqlParameter { ParameterName = "@status", DbType = System.Data.DbType.Boolean, Direction = System.Data.ParameterDirection.Output };
                var out2 = new SqlParameter { ParameterName = "@messages", DbType = System.Data.DbType.String, Size = 800, Direction = System.Data.ParameterDirection.Output };

                ResponseModel response = new ResponseModel();
                db_general.Database.ExecuteSqlCommand("exec dbo.cusp_ins_user @fullName,@email,@phoneNumber,@password,@gender,@birthDate,@status OUTPUT,@messages OUTPUT",
                    parameters: new[] { in1, in2, in3, in4, in5, in6, out1, out2 });

                response.status = (bool)out1.Value;
                response.messages = (string)out2.Value;

                return Json(new { status = response.status, messages = response.messages });

            }
            catch (Exception ex)
            {
                tbl_error_log.insertErrorLog(db_general, register.email, route.action, route.controller, ex.Message);
                return Json(new { status = false, remarks = "Failed connecting to server", err = ex.ToString() });
            }
        }
    }
}