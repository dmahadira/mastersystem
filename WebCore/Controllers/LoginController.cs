using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Entities.General;
using Helpers;
using Models.General;
using System.Web.Http.Cors;

namespace Controllers
{
    [Route("api/[controller]")] 
    // [EnableCors(origins: "http://localhost:5000", headers: "*", methods: "*")]
    public class LoginController : Controller
    {
        private GeneralContext db_general;

        public LoginController(GeneralContext db_general)
        {
            this.db_general = db_general;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("CreateToken")]
        public async Task<JsonResult> CreateToken([FromBody] LoginModel login)
        {
            RouteDataClass route = RouteDataClass.getRouteData(ControllerContext);
            try
            {
                var userdata = tbl_user.getUser(db_general, login.email);
                if (userdata != null)
                {
                    var checkValidUser = tbl_user.checkValidUser(db_general, login);

                    if (checkValidUser)
                    {
                        var tokenString = GenerateToken.BuildToken(userdata);
                        var response = new
                        {
                            token = string.Format("Bearer {0}", tokenString),
                            expired = (int)60 * 60 * 24 * 1,
                            expired_date = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"),
                            pid_user = userdata.pid,
                            email = userdata.email,
                            full_name = userdata.full_name
                        };

                        tbl_user_log.insertLog(db_general, login.email, route.action, route.controller);
                        return Json(new { status = true, messages = "Berhasil Login.", response = response });
                    }
                    else
                    {
                        return Json(new { status = false, messages = "Invalid User, periksa kembali Email dan Password anda!" });
                    }
                }
                else
                {
                    return Json(new { status = false, messages = "Email tersebut belum terdaftar." });
                }
            }
            catch (Exception ex)
            {
                tbl_error_log.insertErrorLog(db_general, login.email, route.action, route.controller, ex.Message);
                return Json(new { status = false, remarks = "Failed connecting to server", err = ex.ToString() });
            }
        }
    }
}