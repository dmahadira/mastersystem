
using Microsoft.AspNetCore.Mvc;

namespace Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        [Route("Account/Login")]
        public IActionResult Login()
        {
            return Json("Invalid-Token");
        }
    }
}