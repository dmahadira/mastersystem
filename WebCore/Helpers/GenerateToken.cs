using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Entities;
using Entities.General;

namespace Helpers
{
    public class GenerateToken
    {
        public static string BuildToken(tbl_user user)
        {
            var now = DateTime.UtcNow;
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(new BaseUrls().getJwtKey()));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Email, "p" + user.email),
                    new Claim(JwtRegisteredClaimNames.NameId, user.pid),
                    new Claim(JwtRegisteredClaimNames.GivenName, user.full_name),
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.email)
                };

            var token = new JwtSecurityToken(new BaseUrls().getJwtIssuer(),
                                new BaseUrls().getJwtAudience(),
                                claims,
                                notBefore: now,
                                expires: DateTime.Now.AddDays(1),
                                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}