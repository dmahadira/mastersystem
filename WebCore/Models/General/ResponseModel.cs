namespace Models.General
{
    public class ResponseModel
    {
        public bool status { get; set; }
        public string messages { get; set; }
    }
}