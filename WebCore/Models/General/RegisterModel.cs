using System.ComponentModel.DataAnnotations;

namespace Models.General
{
    public class RegisterModel
    {   
        [Required (ErrorMessage = "Email is required.")]
        public string email { get; set; }
        [Required (ErrorMessage = "Password is required.")]
        public string password { get; set; }
        [Required (ErrorMessage = "Name is required.")]
        public string full_name { get; set; }
        [Required (ErrorMessage = "Gender is required.")]
        public string gender { get; set; }
        [Required (ErrorMessage = "Birthdate is required.")]
        public string birth_date { get; set; }
        [Required (ErrorMessage = "Phone Number is required.")]
        public string phone_number { get; set; }
    }
}