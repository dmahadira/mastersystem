using System;
using Microsoft.AspNetCore.Mvc;

namespace Models.General
{
    public class RouteDataClass
    {
        public string action { get; set; }
        public string controller { get; set; }

        public static RouteDataClass getRouteData (ControllerContext controllerContext) {
            try
            {
                return new RouteDataClass() {
                    action = controllerContext.RouteData.Values["action"].ToString(),
                    controller = controllerContext.RouteData.Values["controller"].ToString()
                };
            }
            catch (Exception)
            {
                return new RouteDataClass ();
            }
        }
    }
}