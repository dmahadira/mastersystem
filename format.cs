 private MenuLeftClass menuLeftClass = new MenuLeftClass();
        private string iStrSessNRP = string.Empty;
        private string iStrSessDept = string.Empty;
        private string iStrSessGPID = string.Empty;

        // GET: Menu
        public ActionResult Index()
        {
            if (Session["PNRP"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            this.pv_CustLoadSession();
            ViewBag.GPID = iStrSessGPID;
            ViewBag.leftMenu = loadMenu();




            return View();
        }

        private string loadMenu()
        {
            this.pv_CustLoadSession();
            string pathParent = Url.Content("~").Substring(0, Url.Content("~").Length - 1);

            if (Session["leftMenu"] == null)
            {
                Session["leftMenu"] = menuLeftClass.recursiveMenu(pathParent, Convert.ToInt32(iStrSessGPID));
            }
            return (string)Session["leftMenu"];
        }

        private void pv_CustLoadSession()
        {
            iStrSessNRP = (string)Session["PNRP"];
            iStrSessDept = (string)Session["Dept"];
            iStrSessGPID = (string)Session["GP"];
        }